﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PushMagnet : MonoBehaviour
{
	public float magnetism;
    public GameObject player;
	private Rigidbody playerBody;
	private Vector3 position;
	private Vector3 playerPosition;
	private Vector3 difference;
	
	void Start() {
		playerBody = player.GetComponent<Rigidbody>();
		position = transform.position;
		playerPosition = player.GetComponent<Transform>().position;
		difference = new Vector3(position.x, 0, position.z);
	}
	void FixedUpdate(){
		position = transform.position;
		
		playerPosition = player.GetComponent<Transform>().position;
		
		difference = new Vector3(
		position.x - playerPosition.x,
		position.y - playerPosition.y,
		position.z - playerPosition.z);
		
		playerBody.AddForceAtPosition(difference * -1.0f *magnetism, playerPosition);
	}
}